package ru.specialist.lab1_1;

public class Point extends Shape {
    Coords coords = new Coords();
    
    private int x;
    private int y;
    
    public Point(int x, int y) {
        this.coords.setX(x);
        this.coords.setY(y);
    }

    public void setX(int x) {
        this.coords.setX(x);
    }

    public void setY(int y) {
        this.coords.setY(y);
    }
    
    @Override
    public void draw() {
        System.out.println("Point with coords: X = " + coords.getX() + " Y = " + coords.getY());
    }
}
