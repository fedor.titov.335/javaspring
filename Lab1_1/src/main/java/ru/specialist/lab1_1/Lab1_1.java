package ru.specialist.lab1_1;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Lab1_1 {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        
        context.getBean(Point.class).draw();
        context.getBean(Circle.class).draw();
        
//        Point point = context.getBean(Point.class);
//        Circle circle = context.getBean(Circle.class);
//
//        point.setX(2);
//        point.setY(5);
//        point.draw();
//        
//        circle.setCoords(new Coords(3, 9));
//        circle.setRadius(22);
//        circle.draw();
        
        context.close();
    }
}
