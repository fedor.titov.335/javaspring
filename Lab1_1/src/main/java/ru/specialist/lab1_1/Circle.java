package ru.specialist.lab1_1;

public class Circle extends Shape {
    Coords center = new Coords();
    private int radius;

    public Circle() {
    }
    
    public Coords getCoords() {
        return center;
    }

    public void setCoords(Coords coords) {
        this.center = coords;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }
    
    @Override
    public void draw() {
        System.out.println("Circle with coords center: X = " + center.getX() + " Y = " + center.getY() + 
                " R = " + radius);
    }
}
