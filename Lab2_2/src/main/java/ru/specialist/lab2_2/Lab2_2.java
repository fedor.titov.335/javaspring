package ru.specialist.lab2_2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Lab2_2 {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(GraphConfig.class);
        context.getBean(Point.class);
        context.getBean(Circle.class);
        context.getBean(Scene.class).draw();
    }
}
