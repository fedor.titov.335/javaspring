package ru.specialist.lab2_2;

import java.util.ArrayList;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;

@Configuration
public class GraphConfig {
    
    @Bean
    @Scope("prototype")
    @Lazy
    public Coords coords() {
        return new Coords();
    }
    
    @Bean
    @Scope("prototype")
    @Lazy
    public Point point() {
        Point p = new Point(coords());
        p.setColor(Shape.DEFAULT_COLOR);
        scene().Add(p);
        return p;
    }
    
    @Bean
    @Scope("prototype")
    @Lazy
    public Circle circle() {
        Circle c = new Circle(coords());
        c.setColor(Shape.DEFAULT_COLOR);
        c.setR(22);
        scene().Add(c);
        return c;
    }
    
    @Bean
    @Scope("singleton")
    public Scene scene() {
        Scene s = new Scene();
        s.setObjects(new ArrayList<Shape>());
//        s.getObjects().add(point());
//        s.getObjects().add(circle());
        return s;
    }
}
