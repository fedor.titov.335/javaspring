package ru.specialist.lab2_2;

import java.util.List;

public class Scene {
    private List<Shape> objects;

    public List<Shape> getObjects() {
        return objects;
    }

    public void setObjects(List<Shape> objects) {
        this.objects = objects;
    }

    public void draw() {
        for (Shape s : getObjects()) {
            s.draw();
        }
    }
    
    public void Add(Shape objects){
        this.objects.add(objects);
    }
}
