package ru.specialist.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


public interface CourseRepository extends CrudRepository<Course, Integer> {
	List<Course> findAll();
	@Query("SELECT c FROM Course c WHERE c.title LIKE :search") // JPQL
	List<Course> findByTitle(@Param("search") String search);

	
	@Query("SELECT c FROM Course c WHERE c.length <= :mLength") // JPQL
	List<Course> findShortCourses(@Param("mLength") int maxLength);
	
	@Modifying
	@Query("update Course c set c.length = :nLength where c.length = :oLength")
	int incrementLength(@Param("oLength") int oldLength, 
			@Param("nLength") int newLength);
/*	
	// courses.length <= 24
	//@Query("SELECT c FROM Course c WHERE c.length <= 24") // JPQL
//	@Query(value = "SELECT * FROM Course c WHERE c.length <= 24",
//			nativeQuery = true) // SQL
	
	// indexed params
	//@Query("SELECT c FROM Course c WHERE c.length <= ?1") // JPQL
	
	List<Course> findShortCourses(@Param("mLength") int maxLength);
	
	
	*/	
}
