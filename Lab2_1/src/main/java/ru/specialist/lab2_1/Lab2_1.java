package ru.specialist.lab2_1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@ComponentScan("ru.specialist.lab2_1")
public class Lab2_1 {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Lab2_1.class);
        context.getBean(Point.class).draw();
        context.getBean(Circle.class).draw();
    }
}
